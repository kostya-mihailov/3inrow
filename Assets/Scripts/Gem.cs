﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class Gem : MonoBehaviour {

	// Hold Gem position in Grid
	private int i,j;

	private bool mIsSelected = false;
	private IGem mListener;
	private int mType;

	private bool isGemNeedAnimated = false;
	private float x,y; // Move to x,y varible

	List<BaseGemAnimation> animList = new List<BaseGemAnimation> ();

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		/*
		transform.Translate (Vector2.right * Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime);
		transform.Translate (Vector2.up * Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime);*/
		if (transform.position.x == x && transform.position.y == y)
			isGemNeedAnimated = false;
		else if (isGemNeedAnimated){
			Vector2 targetPos = new Vector2(x, y);
			transform.position = Vector2.Lerp(transform.position, targetPos, Time.deltaTime * 5);
		}

	}

	public void OnMouseDown () {
		if (mIsSelected)
			this.mListener.UnSelected (this.i, this.j);
		else
			this.mListener.Selected (this.i, this.j);
	}

	public void OnMouseUp() {
	}

	// Коллбек для событий Элементе
	public void SetIGemListener(IGem listener) {
		this.mListener = listener;
	}

	public void SetSelected(bool value) {
		this.mIsSelected = value;
	}

	// Метод устновки позиции элемента
	public void setPosition(int i, int j) {
		this.i = i;
		this.j = j;
	}

	public int Geti() {
		return this.i;
	}

	public int Getj() {
		return this.j;
	}

	public void setType(int type) {
		this.mType = type;
	}

	public int getGemType() {
		return this.mType;
	}

	public void startMovement(float x, float y) {
		MoveGemAnimation moveAnim = new MoveGemAnimation ();
		moveAnim.targetPos = new Vector2 (x, y);
		moveAnim.type = AnimTypes.Move;

		animList.Add (moveAnim);
	}

	public void startAnimation() { }

	public void stopAnimation() { }

	public interface IGem {
		void Selected(int i, int j);
		void UnSelected(int i, int j);
	}

}
