﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour, Gem.IGem {
	public int height = 5;
	public int width = 5;

	public List<GameObject> gems;

	private List<List<GameObject>> _field = new List<List<GameObject>> ();
	private int _beginPosX, _beginPosY;


	private Gem _selectedItem = null;
	private Gem _second_Selected_Item = null;

	private List<Gem> mItemsForRemove = new List<Gem>();

	void Awake() {

		int i = 0;
		int j = 0;

		_beginPosX = (width / 2) * (-1);
		_beginPosY = (height / 2);

		for (int y = _beginPosY; y > (-1) * _beginPosY; y--) {

			List<GameObject> temp = new List<GameObject>();

			for (int x = _beginPosX; x < (-1) * _beginPosX; x++) {
				int prefPos = Random.Range(0, gems.Count);

				GameObject gem = (GameObject) Instantiate(gems[prefPos]);
				gem.transform.position = new Vector2(x + 0.5f , y - 0.5f);
				temp.Add(gem);

				Gem g1 = (Gem) gem.GetComponent(typeof(Gem));
				g1.setType(prefPos);
				g1.SetIGemListener(this);
				g1.setPosition(i, j);
				j++;
			}

			_field.Add(temp);

			j = 0;
			i++;
		}


	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		FindAndRemoveAll3InRowGems ();

		if (mItemsForRemove.Count != 0) {
			RemoveItems ();
			MoveAllItems ();
			AddItems ();
		} else {
			// Wait user reaction
		}
	}

	#region IGem implementation

	public void Selected (int i, int j)
	{
		if (_selectedItem != null) {
			// Проверить рядом ли элементы
			if (CheckedItem(i, j)) {
				_second_Selected_Item = (Gem)(this._field [i] [j]).GetComponent (typeof(Gem));
				moveItem(_selectedItem, _second_Selected_Item);

				_selectedItem.SetSelected(false);
				_selectedItem = null;
				_second_Selected_Item = null;
			}
			else {
				_selectedItem.SetSelected(false);
				_selectedItem = null;
			}				
		} else {
			_selectedItem = (Gem)(this._field [i] [j]).GetComponent (typeof(Gem));
			_selectedItem.SetSelected(true);
		}
	}

	private bool CheckedItem(int i, int j) {
		int sdvigX = Mathf.Abs (_selectedItem.Geti () - i);
		int sdvigY = Mathf.Abs (_selectedItem.Getj () - j);

		// Диагональный ли элемент
		if (sdvigX == sdvigY && sdvigX == 1)
			return false;
		else {
			if (sdvigX == 0 && sdvigY == 1) {
				return true;
			} else if (sdvigY == 0 && sdvigX == 1) {
				return true;
			} else {
				// Непонятная фигня
				return false;
			}
		}
	}

	private void moveItem(Gem f_item, Gem s_item) {
		float f_item_xPos = f_item.transform.position.x;
		float f_item_yPos = f_item.transform.position.y;
		float s_item_xPos = s_item.transform.position.x;
		float s_item_yPos = s_item.transform.position.y;

		// Change positions
		int fX, fY, sX, sY;
		
		fX = f_item.Geti ();
		fY = f_item.Getj ();
		
		sX = s_item.Geti ();
		sY = s_item.Getj ();

		GameObject fO = _field[fX] [fY];
		GameObject sO = _field[sX] [sY];

		f_item.startMovement (s_item_xPos, s_item_yPos);
		s_item.startMovement (f_item_xPos, f_item_yPos);

		_field [sX] [sY] = fO;
		_field [fX] [fY] = sO;

		f_item.setPosition (sX, sY);
		s_item.setPosition (fX, fY);
/*
		CheckHorizontal (f_item.Geti (), f_item.Getj ());
		CheckVertical (f_item.Geti (), f_item.Getj ());

		CheckHorizontal (s_item.Geti (), s_item.Getj ());
		CheckVertical (s_item.Geti (), s_item.Getj ());

		if (this.mItemsForRemove.Count > 0) {
			// Save position
			RemoveItems();
			MoveAllItems();
			AddItems();
		} else {
			// Roll back position
			f_item.transform.position = new Vector2 (f_item_xPos, f_item_yPos);
			s_item.transform.position = new Vector2 (s_item_xPos, s_item_yPos);
			
			_field [sX] [sY] = sO;
			_field [fX] [fY] = fO;

			f_item.setPosition (fX, fY);
			s_item.setPosition (sX, sY);
		}*/
	}

	public void UnSelected (int i, int j)
	{
		((Gem)(this._field [i] [j]).GetComponent (typeof(Gem))).SetSelected(false);
		_selectedItem = null;
	}

	#endregion

	#region Remove Test

	private void RemoveItems() {

		foreach (Gem item in this.mItemsForRemove) {
			int i = item.Geti();
			int j = item.Getj();

			Destroy(this._field [i] [j], 0.0f);
			this._field [i] [j] = null;
		}

		this.mItemsForRemove.Clear ();
	}

	#endregion

	#region Check Field

	// Check vertical
	public void CheckVertical(int i, int j) {
		List<Gem> results = new List<Gem> ();
		Gem checkedGem = (Gem)(this._field [i] [j]).GetComponent (typeof(Gem));
		int type = checkedGem.getGemType ();

		int xPos = i;
		int gemCount = 1;
		results.Add (checkedGem); // Add first checked item

		// Check upper from item
		while (xPos > 0) {
			xPos--;
			Gem topGem = (Gem)(this._field [xPos] [j]).GetComponent (typeof(Gem));

			if (topGem.getGemType() == type) {
				results.Add(topGem);
				gemCount++;
			} else {
				break;
			}
		}

		// Check bottom from item
		xPos = i;
		while (xPos < height - 1) {
			xPos++;
			Gem bottomGem = (Gem)(this._field [xPos] [j]).GetComponent (typeof(Gem));

			if (bottomGem.getGemType () == type) {
				results.Add(bottomGem);
				gemCount++;
			} else {
				break;
			}


		}

		if (gemCount > 2)
			this.mItemsForRemove.AddRange (results);
	}

	// Check horizontal
	public void CheckHorizontal(int i, int j) {
		List<Gem> results = new List<Gem> ();
		Gem checkedGem = (Gem)(this._field [i] [j]).GetComponent (typeof(Gem));
		int type = checkedGem.getGemType ();

		int yPos = j;
		int gemCount = 1;
		results.Add (checkedGem); // Add first checked item

		// Check left side from item
		while (yPos > 0) {
			yPos--;
			Gem leftGem = (Gem)(this._field [i] [yPos]).GetComponent (typeof(Gem));

			if (leftGem.getGemType() == type) {
				results.Add(leftGem);
				gemCount++;
			} else {
				break;
			}
		}

		// Check right side from item
		yPos = j;
		while (yPos < width - 1) {
			yPos++;
			Gem rightGem = (Gem)(this._field [i] [yPos]).GetComponent (typeof(Gem));

			if (rightGem.getGemType() == type) {
				results.Add(rightGem);
				gemCount++;
			} else {
				break;
			}
		}

		if (gemCount > 2)
			this.mItemsForRemove.AddRange (results);
	}

	#endregion

	private void FindAndRemoveAll3InRowGems() {
		foreach (List<GameObject> gList in _field) {
			foreach (GameObject item in gList) {
				Gem gItem = (Gem) item.GetComponent(typeof(Gem));
				CheckHorizontal(gItem.Geti(), gItem.Getj());
				CheckVertical(gItem.Geti(), gItem.Getj());
			}
		}
	}

	#region MoveDown Gems

	private void FillEmptyPos(List<int> rowPos, List<int> itemBeginPos) {
		if (rowPos.Count != itemBeginPos.Count)
			return;

		for (int i = 0; i < rowPos.Count; i++) {

		}
	}

	private void MoveRow(int rowPos, int itemPos) {
		if (rowPos > width)
			return;

		// Вычислить на сколько позиций надо сместить элементы
		int movePos = (height -1) - itemPos;

		for (int i = itemPos; i <= 0; i--) {
			Vector2 oldPos = _field[i][rowPos].transform.position;
			oldPos.y -= rowPos;

			_field[i][rowPos].transform.position = oldPos;
			((Gem)(this._field [i] [rowPos]).GetComponent (typeof(Gem))).setPosition(i, rowPos);
		}

	}

	private void MoveAllItems () {
		for (int i = height - 1; i >= 0; i--) {
			for (int j = width - 1; j >= 0; j--) {
				if (_field[i][j] == null) {
					// Find first upper item not null
					int k = i - 1;
					while (k >= 0 && _field[k][j] == null) {
						k--;
					}

					if (k != -1){
						// Нужно двигать вниз
						_field[i][j] = _field[k][j];
						Vector2 pos = _field[i][j].transform.position;

						pos.y -= (i - k);

						_field[i][j].transform.position = pos;
						((Gem)(_field[i][j]).GetComponent (typeof(Gem))).setPosition(i, j);
						_field[k][j] = null;
					}

				}
			}
		}
	}

	private void AddItems() {

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {

				if (_field[i][j] == null) {
					int prefPos = Random.Range(0, gems.Count);
					GameObject gameObj = (GameObject) Instantiate(gems[prefPos]);
					_field[i][j] = gameObj;

					Gem gemObj = (Gem) gameObj.GetComponent(typeof(Gem));
					gemObj.setPosition(i, j);
					gemObj.setType(prefPos);
					gemObj.SetIGemListener(this);

					gameObj.transform.position = new Vector2((_beginPosX + 0.5f) + j, (_beginPosY - 0.5f) - i);
				}

			}
		}

		/*int i = 0;
		int j = 0;
		
		_beginPosX = (width / 2) * (-1);
		_beginPosY = (height / 2);
		
		for (int y = _beginPosY; y > (-1) * _beginPosY; y--) {			
			for (int x = _beginPosX; x < (-1) * _beginPosX; x++) {

				if (_field[][])

				int prefPos = Random.Range(0, gems.Capacity);
				
				GameObject gem = (GameObject) Instantiate(gems[prefPos]);
				gem.transform.position = new Vector2(x + 0.5f , y - 0.5f);
				temp.Add(gem);
				
				Gem g1 = (Gem) gem.GetComponent(typeof(Gem));
				g1.setType(prefPos);
				g1.SetIGemListener(this);
				g1.setPosition(i, j);
				j++;
			}
			
			_field.Add(temp);
			
			j = 0;
			i++;
		}*/

	}


	#endregion
}
